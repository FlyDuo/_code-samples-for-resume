<?php $thisPage="home"; ?>
<?php $fullPageTitle="Full page Title here.."; ?>

<?php include("inc/head/html-page-head-primary.inc.php"); ?>

</head>

<body class="home">
<?php include("inc/body/google-analytics-tracking.inc.php");//GA: use this include or remove include and uncomment code in /inc/close/footer.inc instead. ?>
<!-- FlyDuo -->
<div class="FlyDuo">
		     
	 <!-- browser-body-container -->
	 <div class="browser-body-container">

	 	
	 	<!-- .site-nav -->
		 <div class="site-nav">
				<section class="menu">
					<nav>
			            <ul>
			                <li><a href="/">Home</a></li>
			                <li><a href="/updates">Updates</a></li>
			            </ul>
			        </nav>
				</section>
			</div><!-- /.site-nav -->


							 
				<!-- site-container -->
				<div class="site-container">
          
		  			
		  		<!-- .header-wrapper -->
		          <div class="header-wrapper">
			          <header>
			          <!-- .banner -->
			            <div role="banner" class="banner">
			            	<!-- .masthead -->
			                <div class="masthead">
			                	<figure id="header-banner"> 
			                   		<img src="_assets/img/the-header.jpg" alt="Illustration of buble letters. Reads: ..." title="Title.." />
			                   	</figure>
			                </div><!-- /.masthead -->
			            </div><!--/.banner-->
			          </header>
		          </div><!-- /.header-wrapper -->


		          <!-- clearfix -->
				 <div class="clearfix"></div><!-- /.clearfix -->


		          <!-- .boiler-wrapper -->
		          <div class="boiler-wrapper">
		          <img src="_assets/img/boiler-image.jpg" alt="Scene of illuminated (illuniated like firefly's) Butterfly's flying out of a mason jar at night." title="" />
		          </div><!-- /.boiler-wrapper -->
						 
					
          
          <!-- content-wrapper -->
					<div class="content-wrapper">

						<!-- .row -->
						<div class="row">

							<!-- .left-col -->
							<div class="left-col">
								<article class="intro">
									<section>
										<p>
											Info Paragraph Goes Here. Possibly explain the situation, what the site is for etc.  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar nisi id convallis imperdiet. Etiam lacinia odio sit amet elementum malesuada. Vivamus tempor mattis aliquam. Maecenas gravida, felis eu fringilla vestibulum, augue dui consequat nisl.
										</p>
									</section>
								</article>

								<section class="link">
									<p>
										<a href="" title="">Follow Updates Here...</a>
									</p>								
								</section>							
							</div><!-- /.left-col -->

							<!-- .right-col -->
							<div class="right-col">
								<article class="contact">
									<section>
										<p>
											If you would like to send cards please send to:
										</p>

										<ul>
											<li>The Family's Name</li>
											<li>Street Address</li>
											<li>City, ST 12345</li>
										</ul>
									</section>
								</article>
							</div><!-- /.right-col -->
						</div><!-- /.row -->

						<!-- .row.guest-book -->
						<div class="row guest-book">
							<section class="link-img">
								<a href="" title="Follow Updates Here..."><img src="_assets/img/guest-book-bg.jpg" alt="Click to follow updates" title="Click to follow updates" /></a>
							</section>
						</div><!-- /.row.guest-book -->
								
							
					</div><!--/.content-wrapper -->
          
          
					<!-- clearfix -->
					<div class="clearfix"></div><!-- /.clearfix -->
					
					<?php include("inc/body/footer.inc.php"); ?>
					
																		 
						</div><!-- /.site-container -->		     		     
						 						
	 </div><!-- /.browser-body-container -->
</div><!-- /.FlyDuo -->

<?php include("inc/close/scripts-close.inc.php"); ?>

</body>
</html>