<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->
<html dir="ltr" lang="EN">

<head id="" data-template-set="html5-reset">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		
	<link type="text/plain" rel="author" href="humans.txt" />
		
	<?php /* <meta name="viewport" content="width=device-width, maximum-scale=1.0" /> */ ?>

	
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

	<link rel="shortcut icon" href="_assets/img/icons/favicon.png">
	<!-- This is the traditional favicon.
		 - size: 16x16 or 32x32
		 - transparency is OK
		 - see wikipedia for info on browser support: http://mky.be/favicon/ -->
		 
	<link rel="apple-touch-icon" href="_assets/img/icons/apple-touch-icon.png">
	<!-- The is the icon for iOS's Web Clip.
		 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
		 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
		 - Transparency is not recommended (iOS will put a black BG behind the icon) -->


<title>Project Name<?php if (isset ($fullPageTitle) && ($fullPageTitle!="")) {
  				echo " :: $fullPageTitle";
				} ?></title>


<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->				

<?php //meta ?>
<meta name="google-site-verification" content="" />

<meta name="MSSmartTagsPreventParsing" content="true" />

<meta http-equiv="expires" content="-1" />
<meta http-equiv= "pragma" content="no-cache" />

<meta name="web_application_developer" content="Reuben N' Sherri - The FlyDuo at Fly Media Productions, LLC" />

<meta name="description" content="company website description" />
<meta name="keywords" content="company, name" />


<!-- Dublin Core Metadata : http://dublincore.org/ -->
<meta name="DC.title" content="Company Name or Project name"><?php //Project Name ?>
<meta name="DC.subject" content="What is this project or company all about"><?php //What you're about. ?>
<meta name="DC.creator" content="The Client's Name Here"><?php //Who made this page's content :: The CLIENT's name here. ?>



<?php //styles ?>
<link rel="Stylesheet" href="_assets/css/global.css" type="text/css" media="screen" />
<link rel="Stylesheet" href="_assets/css/print.css" type="text/css" media="print" /> 



<!-- web-fonts -->
<link href='https://fonts.googleapis.com/css?family=Nothing+You+Could+Do' rel='stylesheet' type='text/css'>
<!-- /web-fonts -->


<?php //browser conditionals, device conditionals bugs ?>
<!--[if lte IE 8]>
<style type="text/css">
body {/*behavior: url(styles/csshover.htc);*/}
</style>
<![endif]-->

<!--[if lte IE 7]>
<style type="text/css">
body {/*behavior: url(styles/csshover.htc);*/}
</style>
<![endif]-->

<!--[if lte IE 6]>
<style type="text/css">
body {/*behavior: url(styles/csshover.htc);*/}
</style>
<![endif]-->



<?php //main scripts	?>
<script type="text/javascript">
	/*  google webfonts  */ 
  WebFontConfig = {
    google: { families: [ 'Nothing+You+Could+Do::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
</script>


<!-- all our JS is at the bottom of the page, except for Modernizr. -->
<script src="_assets/js/tools/modernizr-1.7.min.js"></script>

<!-- fancy box css -->
<link rel="Stylesheet" href="_assets/js/jquery/tools/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css" media="screen" />	

